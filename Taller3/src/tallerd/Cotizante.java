/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerd;

/**
 *
 * @author Administrador
 */
public class Cotizante implements Usuario{
    private String nombre;
    private String cc;
    private String genero;

    public Cotizante(String nombre, String cc, String genero) {
        this.nombre = nombre;
        this.cc = cc;
        this.genero = genero;
    }

    @Override
    public String addI() {
        return nombre+" "+cc+" "+genero+" ";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    
    
}
